## [0.2.4](https://bitbucket.org/GoodRequest/swift/commits/tag/0.2.4)
Released on 2017-02-21.

#### Added
- Package.swift

#### Updated
- source file comment headers

## [0.2.3](https://bitbucket.org/GoodRequest/swift/commits/tag/0.2.3)
Released on 2017-02-21.

#### Added
- CHANGELOG.md
- Code comments.

## [0.2.2](https://bitbucket.org/GoodRequest/swift/commits/tag/0.2.2)
Released on 2017-02-17.

#### Updated
- Request logger improvements.

## [0.2.1](https://bitbucket.org/GoodRequest/swift/commits/tag/0.2.1)
Released on 2017-02-17.

#### Fixed
- Made all functions public.

## [0.2.0](https://bitbucket.org/GoodRequest/swift/commits/tag/0.2.0)
Released on 2017-02-17.

#### Added
- Initial release of **.good**swift.
  - Added by [Marek Spalek](https://bitbucket.org/MarekSpalek/).
