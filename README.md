![.goodswift](https://www.dropbox.com/s/n7scwrla13ym870/goodswift.png?dl=1)


[![Version](https://img.shields.io/cocoapods/v/GoodSwift.svg?style=flat)](http://cocoapods.org/pods/GoodSwift)
[![License](https://img.shields.io/cocoapods/l/GoodSwift.svg?style=flat)](http://cocoapods.org/pods/GoodSwift)
[![Platform](https://img.shields.io/cocoapods/p/GoodSwift.svg?style=flat)](http://cocoapods.org/pods/GoodSwift)

Some good swift extensions, handfully crafted by GoodRequest team.

## Requirements

- iOS 9.0+
- Xcode 8.2+
- Swift 3.0+

## Installation

**.good**swift is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GoodSwift"
```

## Author

Marek Spalek, marek.spalek@goodrequest.com

## License

Smaky is available under the MIT license. See the LICENSE file for more info.
